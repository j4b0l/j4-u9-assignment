#!/usr/bin/env python

import web

urls = (
    '/api/status', 'handler_status',
)

app = web.application(urls, globals())


class handler_status:
    def GET(self):
        output='hello world'
        return output

if __name__ == "__main__":
    app.run()

