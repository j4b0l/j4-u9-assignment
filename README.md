Repository layout:
=================
app - application, including unit tests (utest.py) and integration tests (itest.py)
infrastructure - configurations for Jenkins and test environments

Decisions made (approximately in order of appearance):
======================================================
Introduction
------------
When I think of such projects I usually go with solutions and technologies I
know and am able to troubleshoot quickly. This drove many of my decisions.
At the same time I wouldn't spend too much time on this task, so out of my
initial idea there are still some areas for improvement, I'll mention that
for each part I'm describing.

Hosting service - self-hosted, docker+docker-compose
------------------------------------
Docker was my first choice, rather no-brainer. I can put my repository anywhere
and deploy anywhere, knowing that I have the configuration and two pieces of
software installed on the server (which I already have).
Most of infrastructure is described in configuration, it's possible to easily
move whole project to different server, Jenkins persistance lies in directory
mounted to Docker container, composition of services in designed in the way
that allows further development.

Areas for further improvement:
- each service has simple nginx-based proxy, right now it only takes care of
  simple http authentication, but can be replaced by HAProxy with HADiscovery
  to have painless deploys to production
- Jenkins service still has some area for improvement, few manual steps need
  to be done after deploying it (ssh key generation or installatoin of git
  inside container for Jenkins to work properly)

Continuous integration - Jenkins
--------------------------------
I use Jenkins on my current assignments. Already took part in quite complex 
test/deployment design for full end-to-end testing of multi-component app
(Java+angularJS UI, Erlang backend, ANSI C client). Moreover Jenkins community
provides many useful plugins, out of which I only use few.

Areas for further improvement:
- Jenkins can also work quite well with Docker, creating standarized, really
  clean environment for each buld
- setup of test environments can be also parametrized and passed to Jenkins
  from developer's point of view, using properties files

Language - python
-----------------
Python was also no-brainer in terms of my previous experience and ease of use,
with good base of libraries. I've created similar service recently, with less
logic behind every request, but more varying requests to be handled.

Areas for further improvement:
- unit test and integration test are still to be improved
- decomposition of python objects could be done better to make sure all of
  classes can be properly unit-tested
- there's just one provider for weather

Test suite - python unittest
----------------------------
I've organized test suites in simple unittest modules, not much time as it
appeared.

Areas for further improvement:
- many, tests were usually the last thing to take care of in many projects
  I've taken a part in; none of them was really failed though

CI/CD setup
-----------
Setup is simple and bases on short path: build and run unit tests, deploy
to test environment, run integration test suite, deploy to production.
This path is prepared to be extended by additional test environment
or more extensive integration testing.

Areas for further improvement:
- current test and production environments are setup as single containers
  with proxy on top of that, which can be changed with Docker+HAProxy
- for bigger infrastructure each environment can have own composition
  configuration, with global infrastructure (like Jenkins) delegated to
  global infrastructure repositories
- composition for some of cloud services can be based on current Docker
  containers, with private Docker registry and apropriate configuration
  of pods and services

If I got stuck
--------------
Well, I haven't. I must admit that I just dropped some of the points that should
be discussed with client in terms of real project. But I'll leave that to direct
communication.