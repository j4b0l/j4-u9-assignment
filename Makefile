GOALS = deploy-test deploy-prod
.DEFAULT_GOAL=info
.PHONY: all $(GOALS) info
	CONFIGS = vbs/vbspkg.cfg scripts/settings.inc fig.yml

info:
		@echo "this is just a prototype - no default goal set"

all: info

deploy-test: test-version
		cp -rfT app infrastructure/test-env/data
		cd infrastructure && docker-compose stop u9test u9testproxy && docker-compose rm --force u9test u9testproxy && docker-compose build u9test u9testproxy && docker-compose up -d --no-recreate

deploy-prod: prod-version
		cp -rfT app infrastructure/prod-env/data
		cd infrastructure && docker-compose stop u9app u9appproxy && docker-compose rm --force u9app u9appproxy && docker-compose build u9app u9appproxy && docker-compose up -d --no-recreate

prod-version:
		cd app && echo -e "suffix='`git rev-list HEAD --count`'" >> version.py

test-version:
		cd app && echo -e "suffix='`git rev-parse HEAD`'" >> version.py

test:
		cd app && ./utest.py

