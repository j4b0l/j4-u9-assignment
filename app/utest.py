#!/usr/bin/env python

import unittest
from provider import weather_provider

class stub_weather_provider:
    @staticmethod
    def get_temperature(where):
        if(where=="dummy"):
            return '12.3'
        else:
            return '0'
    @staticmethod
    def get_provider():
        return 'dummy.com'

class weather_providerTest(unittest.TestCase):
    def setUp(self):
        self.prov=weather_provider()
        self.prov.add_provider(stub_weather_provider)
    def tearDown(self):
        del self.prov
    def test_add_provider(self):
        number=len(self.prov.get_providers_list())
        self.assertTrue(self.prov.add_provider(stub_weather_provider))
        self.assertEqual(len(self.prov.get_providers_list()), number+1)
        self.assertTrue(self.prov.add_provider(stub_weather_provider))
        self.assertEqual(len(self.prov.get_providers_list()), number+2)
        self.assertFalse(self.prov.add_provider(str('dummy')))
        self.assertFalse(self.prov.add_provider(1))
    def test_get_temperature(self):
        self.assertEqual(self.prov.get_temperature('dummy'), 12.3)
        self.assertEqual(self.prov.get_temperature('notdummy'), 0)
    def test_get_providers(self):
        self.assertEqual(self.prov.get_providers(), 'dummy.com')
        self.assertTrue(self.prov.add_provider(stub_weather_provider))
        self.assertEqual(self.prov.get_providers(), 'dummy.com,dummy.com')
        self.assertTrue(self.prov.add_provider(stub_weather_provider))
        self.assertEqual(self.prov.get_providers(), 'dummy.com,dummy.com,dummy.com')

if __name__ == '__main__':
        unittest.main()
