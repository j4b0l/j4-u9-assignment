#!/usr/bin/env python

import web
import version
import json
import provider
from openweathermap import owm_weather_provider;

urls = (
    '/api/status', 'handler_status',
    '/api/weather/(.*)', 'handler_weather',
)

app = web.application(urls, globals())
web.config.debug=False

class handler_weather:
    def GET(self, location):
        prov=provider.weather_provider()
        prov.add_provider(owm_weather_provider)
        prov.add_provider(str('dupa'))
        #output='no weather data for '+command
        out_json={}
        out_json['credit']=prov.get_providers()
        out_json['temperature']=prov.get_temperature(location)
        output=json.dumps(out_json)
        return output

class handler_status:
    def GET(self):
        out_json={}
        out_json['version']=str(version.major)+'.'+str(version.minor)+'.'+str(version.suffix)
        output=json.dumps(out_json)
        return output

if __name__ == "__main__":
    app.run()

