#!/usr/bin/env python

import unittest
from provider import weather_provider
import urllib2
import json
import sys

cities=['lodz', 'lodz,poland', 'warsaw', 'warsaw,poland', 'sandiego,us', 'ontario']

if(len(sys.argv)>1):
    test_address=sys.argv.pop(1)
else:
    test_address='127.0.0.1:8080'

testenv_url='http://'+test_address+'/api/'

class weather_providerTest(unittest.TestCase):
    def test_api(self):
        for city in cities:
            full_url=testenv_url+'weather/'+city
            print full_url
            weather_json=urllib2.urlopen(full_url).read()
            weather=json.loads(weather_json)
            self.assertTrue(weather.has_key('temperature'))
            self.assertTrue(weather.has_key('credit'))



if __name__ == '__main__':
        unittest.main()
