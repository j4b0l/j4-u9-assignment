
class weather_provider:
    def __init__(self):
        self.providers=[]
    def get_temperature(self, location):
        if (len(self.providers)>0):
            sum=0
            for p in self.providers:
                sum+=float(p.get_temperature(location))
            return sum/len(self.providers)
        else:
            return 0
    def get_providers(self):
        if (len(self.providers)>0):
            prov_list=[]
            for p in self.providers:
                prov_list.append(str(p.get_provider()))
            return ",".join(prov_list)
        else:
            return ''
    def add_provider(self,p):
        try:
            if(callable(getattr(p,'get_temperature')) and callable(getattr(p,'get_provider'))):
                self.providers.append(p)
                return True
        except AttributeError:
            return False
    def get_providers_list(self):
        return self.providers

