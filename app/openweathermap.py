import urllib2
import json
API_key='ff4561f27516bb3e3a7dccabef4824fc'

class owm_weather_provider:
    @staticmethod
    def get_temperature(where):
        weather_url='http://api.openweathermap.org/data/2.5/weather?q='+str(where)+'&appid=ff4561f27516bb3e3a7dccabef4824fc'
        weather_json=urllib2.urlopen(weather_url).read()
        weather=json.loads(weather_json)
        temperature=float(weather['main']['temp'])-273.15
        return temperature
    @staticmethod
    def get_provider():
        return 'openweathermap.com'



